//furniture add more imgs

const btnLoadMore = $('#loadMore');

$('.furniture__wrapper').slice(0, 6).removeClass('furniture__wrapper_hidden');
if ($('.furniture__wrapper.furniture__wrapper_hidden').length !== 0) {
    $(btnLoadMore ).show();
}

$(btnLoadMore ).on('click', function (event) {
    event.preventDefault();
    $('.furniture__wrapper.furniture__wrapper_hidden').slice(0, 3).removeClass('furniture__wrapper_hidden');
    if ($('.furniture__wrapper.furniture__wrapper_hidden').length === 0) {
        $(btnLoadMore).fadeOut('slow');
    }
});

// filter by cat
// range filter